from django.contrib import admin

from .models import DocFile
# Register your models here.

admin.site.register(DocFile)
