from django.urls import path

from . import views

urlpatterns = [
    path('', views.index),
    path('data/<int:year>/<int:month>/', views.old_data),
    path('data/files/', views.get_data_files),
    path('archives/', views.archives),
    path('file/<int:pk>/', views.open_doc_file),
]
