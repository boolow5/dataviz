from django.shortcuts import render
from django.http import JsonResponse
from django.core import serializers

from .models import DocFile

settings = {
    "subtitle": "Internal Displacement",
    "org_name": "DRC - Danish Refugee Council",
    "site_name": "DRC IDPs Data Visualisation",
    "site_description": "Displacements Monitored by DRC Protection",
    "country": "Somalia",
    "url": "https://drc.ngo",
    "twitter_link": "https://twitter.com/drc_dk",
    "facebook_link": "https://www.facebook.com/danishrefugeecouncil",
    "email": {
        "subject": "Check out this interactive visualization",
        "body": "Please find DRC Somalia - Interactive Internal Displacements Visualization here: https://drc.ngo",
    },
}

# Create your views here.
def index(request):
    defaultDataFile = "static/docmanager/data/DRCProtectionDataset-latest.csv"
    allfiles = DocFile.objects.all()
    last_doc = None
    if len(allfiles) > 0:
        last_doc = allfiles[0]
    else:
        context["error"] = {
            "title": "FILE NOT FOUND!",
            "text": "The dataset file you requested might be deleted or is not available",
        }
        return render(request, "docmanager/pages/error.html", context)
    context = {"settings": settings, "data_file_name": defaultDataFile}
    if not last_doc.proccessed:
        last_doc.process_file()
    if last_doc.csv != '':
        context["data_file_name"] = last_doc.csv
    return render(request, "docmanager/pages/index.html", context)

def open_doc_file(request, pk=0):
    context = {"settings": settings, "data_file_name": "static/docmanager/data/DRCProtectionDataset-latest.csv"}
    try:
        file = DocFile.objects.get(pk=pk)
        if file.proccessed:
            print("file is proccessed")
            context["data_file_name"] = file.csv
            return render(request, "docmanager/pages/index.html", context)
        else:
            print("file is NOT proccessed")
            file.process_file()
            context["data_file_name"] = file.csv
            return render(request, "docmanager/pages/index.html", context)

    except DocFile.DoesNotExist as e:
        print("file is NOT found")
        context["error"] = {
            "title": "FILE NOT FOUND!",
            "text": "The dataset file you requested might be deleted or is not available",
        }
        return render(request, "docmanager/pages/error.html", context)

def archives(request, year=0):
    data = {"docs": []}
    docs = []
    first_proccessed_doc = None
    if year > 0:
        docs = DocFile.objects.filter(created_at__year=year)
    else:
        docs = DocFile.objects.all()
    for doc in docs:
        if first_proccessed_doc is None and doc.proccessed:
            first_proccessed_doc = doc
        if not doc.proccessed:
            doc.process_file()
        # serializers.serialize('json', doc)
        print('without extension: {}'.format(remove_extension(doc.csv)))
        data["docs"] += [{
            "id": doc.pk,
            "created_at": doc.created_at,
            "url": doc.csv,
            "dir": doc.get_dir(media=True),
            "year": doc.created_at.year,
            "month": doc.created_at.month,
            "file_name":remove_extension(doc.get_csv_name()),
        }]
    last_file_url = ''
    if first_proccessed_doc:
        last_file_url = first_proccessed_doc.csv
    else:
        last_file_url = 'static/docmanager/data/DRCProtectionDataset-latest.csv'

    context = {"settings": settings, "data_file_name": last_file_url, "data": data, "page": "archives"}
    return render(request, "docmanager/pages/archives.html", context)

def remove_extension(file_name):
    print('remove_extension')
    if len(file_name) < 3:
        return file_name
    parts = str(file_name).split('.')
    if len(parts) > 1:
        return '.'.join(parts[:len(parts)-1])
    else:
        return file_name

def old_data(request, year=2018, month=1):
    file = DocFile.objects.get(created_at__year=year, created_at__month=month)[0]
    error = ''
    if file.csv == '':
        error = 'No data file found'
    context = {"settings": settings, "data_file_name": file.csv, 'error': error}
    return render(request, "docmanager/pages/index.html", context)

def get_data_files(request):
    data = {"docs": []}
    docs = DocFile.objects.all()
    for doc in docs:
        # serializers.serialize('json', doc)
        data["docs"] += [{
            "created_at": doc.created_at,
            "url": doc.csv,
            "dir": doc.get_dir(media=True),
        }]
    return JsonResponse({"docs": data})

def process_all_files():
    docs = DocFile.objects.filter(proccessed=False)
    for doc in docs:
        doc.process_file()
        if doc.proccessed and len(doc.csv) > 0:
            doc.save()
    print('done processing {} file(s)'.format(len(docs)))
