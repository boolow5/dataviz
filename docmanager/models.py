import os
from django.db import models
from django.utils import timezone
import openpyxl
import datetime

month_names = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

# Create your models here.
class DocFile(models.Model):
    """represents document."""
    file = models.FileField(upload_to='uploads/%Y/%m/%d/')
    proccessed = models.BooleanField(default=False)
    csv = models.URLField(max_length=300, default='', blank=True)
    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return str(self.file.name)

    def get_csv_name(self):
        parts = str(self.file.path).split('/')
        csv_path = ''
        if len(parts) > 1:
            name = parts[-1]
            parts = name.split('.')
            last_index = len(parts) - 1
            if last_index > -1:
                return '.'.join(parts[:last_index]) + '.csv'
        else:
            return ''
    def get_dir(self, media=False):
        parts = []
        if media:
            parts = str(self.file.url).split('/')
        else:
            parts = str(self.file.path).split('/')
        if len(parts) > 1:
            return '/'.join(parts[:len(parts)-1])
        elif len(parts) > 0:
            return parts[0]
        return ''

    def process_file(self, auto_save=True):
        print("processing file " + self.file.path)
        print(self.file)
        if len(self.file.path) > 0:
            workbook = openpyxl.load_workbook(self.file.path)
            sheets = workbook.get_sheet_names()
            print('foundt {} sheets'.format(len(sheets)))
            id = -1
            rows_str = ''
            for sh, name in enumerate(sheets):
                print('sheet {}.'.format(sh + 1))
                sheet = workbook.get_sheet_by_name(name)
                # each row
                for i, row in enumerate(sheet.iter_rows()):
                    id += 1
                    yr = 0
                    monthnum = 0
                    monthname = ''
                    monthend = None
                    cregion = ''
                    cdistrict = ''
                    pregion = ''
                    pdistrict = ''
                    creason = ''
                    tpeople = ''
                    yrmonthnam = ''
                    yrmonthnum = ''
                    value = ''
                    if i == 0:
                        rows_str = 'id,yr,monthnum,monthname,monthend,cregion,cdistrict,pregion,pdistrict,creason,tpeople,yrmonthnam,yrmonthnum\n'
                        continue
                    # each cell
                    # print('\nrow: {}:'.format(id))
                    for j, cell in enumerate(row):
                        if j == 0:
                            monthend = cell.value
                            # print('type(monthend) == {}'.format(type(monthend)))
                            if type(monthend) is datetime.datetime:
                                monthnum = monthend.month
                                if monthnum < 13:
                                    monthname = str(month_names[monthnum - 1])[:3]
                                yr = monthend.year
                                yrmonthnam = "{} '{}".format(monthname, monthend.strftime('%y'))
                                yrmonthnum = monthend.strftime('%Y-%m')
                                monthend = monthend.strftime("%d/%m/%Y")
                            else:
                                print("monthend is not a date, it's {} type".format(type(monthend)))
                        elif j == 1:
                            cregion = cell.value
                        elif j == 2:
                            cdistrict = cell.value
                        elif j == 3:
                            pregion = cell.value
                        elif j == 4:
                            pdistrict = cell.value
                        elif j == 5:
                            creason = cell.value
                        elif j == 6:
                            tpeople = cell.value

                    rows_str += "{id},{yr},{monthnum},{monthname},{monthend},{cregion},{cdistrict},{pregion},{pdistrict},{creason},{tpeople},{yrmonthnam},{yrmonthnum}\n".format(id=id,yr=yr,monthnum=monthnum,monthname=monthname,monthend=monthend,cregion=cregion,cdistrict=cdistrict,pregion=pregion,pdistrict=pdistrict,creason=creason,tpeople=tpeople,yrmonthnam=yrmonthnam,yrmonthnum=yrmonthnum)
                fname = self.get_csv_name()
                full_path = os.path.join(self.get_dir(), fname)
                print('full_path: {}'.format(full_path))
                success = save_str_to_file(rows_str, full_path)
                if success:
                    self.proccessed = True
                    self.csv = os.path.join(self.get_dir(media=True), fname)
                if auto_save:
                    self.save()
                print("file processing completed. {} lines and {} characters".format(id, len(rows_str)))

def save_str_to_file(s='', path=''):
    print("saving {} characters to {}".format(len(s), path))
    if len(s) < 1:
        print('nothing to write to file')
    elif len(path) < 1:
        print('unknown path to write to file')
        return False
    else:
        with open(path, 'w+') as file:
            file.write(s)
            print('done writing to file!')
            return True
